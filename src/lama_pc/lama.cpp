/*
 * lama.cpp
 * 
 * Copyright 2015 2014ws_muehle <2014ws_muehle@rodney>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include<iostream>

#include"randomaiarm.h"
#include"freedomaiarm.h"
#include"board.h"
#include"screenhuman.h"
#include"randomscreenai.h"
#include"game.h"
#include"boardinterface.h"
#include"arminterface.h"
#include"human.h"

using namespace std;

int main(int argc, char **argv)
{
	//atexit(clean); // call to a Function to clean the board
	cout << "start" << endl;

	arminterface * ai = new arminterface();
	//if (ai->connect("/dev/ttyS0")) {
	if (ai->connect((char*)"/dev/ttyUSB0")) {
		std::cout << "arm connected\n";
	}
	else {
		std::cout << "cannot connect to arm\n";
	}

	boardinterface * bi = new boardinterface();
	cout << "boardinterface made" << endl;
	//if (bi->connect("/dev/ttyUSB0")) {
	if (bi->connect((char*)"/dev/ttyS0")) {
		cout << "board connected\n";
	}
	else {
		cout << "cannot connect to board\n";
	}

	//Player * One = new screenhuman();
	Player * One = new Human(bi);
	//Player * One = new randomAIArm(ai, bi);
	//Player * One = new randomScreenAI();
	//Player * Two = new screenhuman();
	//Player * Two = new Human(bi);
	//Player * Two = new randomAIArm(ai, bi);
	Player * Two = new freedomAIArm(ai, bi);
	//Player * Two = new randomScreenAI();

	game spiel(One, Two);

	while(true) {
		//cout << fieldToStr(PLAYER_1);
		//cout << One->whoAmI() << endl;
		spiel.nextTurn();
	}

	delete One;
	delete Two;
	return 0;
}

