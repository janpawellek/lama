#ifndef BOARDINTERFACE_H
#define BOARDINTERFACE_H

#include"board.h"
#include"player.h"
#include"qextserialport.h"

# include <iostream>
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <termios.h>
# include <sys/select.h>

# define TERM_SPEED B9600      /* Bit/Sek */

/* Braucht Signale oder ähnliches um faltsches setzen des menschen
 * und möglichkeit einen stein zu schlagen das der Mensch
 * nun drann ist usw am board zu signalisieren
 * villeicht:
 */

class boardinterface {
	public:
		// constructor
		boardinterface();

		// destructor
		~boardinterface();

		// conmect to a devicea (e.g. "/dev/ttyS0")
		bool connect(char* device);

		// disconnect
		bool disconnect();

		// waits for the next board change
		// and saves the new status
		Board * waitBoardChange();
		// waits for change to happen
		// befor return
		Board * waitBoardNextChange();
		// gets the board status now 
		Board * getBoardStatus();

		// guesses the boards status based on which player
		// has made the last turn and and how it was befor
		Board * calculateRealBoard(Board*, fieldstate);

	private:
		// QextSerialPort sollte die gesamte comunikation mit dem
		// serielen port übernehmen
		QextSerialPort * port;

		void sendChar(char);
		// Read the board and return a Board class (with PLAYER_UNKNOWN
		// states if a field is set)
		Board * readBoard();

		Board * board;

		int charToNum(int, char);

		// Funktionen, um von µc-Koordinaten zu
		// PC-Koordinaten umzurechnen
		int getPcCoordX(int mcCoord);
		int getPcCoordY(int mcCoord);
		int getPcCoordZ(int mcCoord);
};

#endif // BOARDINTERFACE_H
