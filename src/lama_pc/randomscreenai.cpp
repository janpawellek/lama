#include"randomscreenai.h"

randomScreenAI::randomScreenAI()
	: Player() {
}

randomScreenAI::randomScreenAI(Board * f)
	: Player(f) {
}

void randomScreenAI::turn(phase moment) {
	srand(time(NULL));
	switch(moment) {
		case SET:
			this->turnSet();
			break;
		case MOVE:
			this->turnMove();
			break;
		case JUMP:
			this->turnJump();
			break;
		case WIN:
			this->turnWin();
			break;
		case LOSE:
			this->turnLose();
			break;
		default:
			break;
	}
	return;
}

void randomScreenAI::beat() {
	std::vector<std::vector<int> > enemy = this->field->allOfPlayer(
			this->field->enemyOf(
				this->whoAmI()
			)
		);
	bool found = false;
	int random;
	while(not found) {
		random = rand() % enemy.size();
		int x = enemy[random][0];
		int y = enemy[random][1];
		int z = enemy[random][2];
		if(this->field->isInMill(x, y, z)) {
			enemy.erase(enemy.begin() + random);
			continue;
		}
		else {
			(*this->field)[x][y][z] = EMPTY;
			return;
		}
	}
}

void randomScreenAI::turnSet() {
	std::vector<std::vector<int> > empty = this->field->allOfPlayer(
			EMPTY
		);
	int random = rand() % empty.size();
	int x = empty[random][0];
	int y = empty[random][1];
	int z = empty[random][2];
	(*this->field)[x][y][z] = this->whoAmI();
	return;
}

void randomScreenAI::turnMove() {
	std::vector<std::vector<int> > me = this->field->allOfPlayer(
			this->whoAmI()
		);
	std::vector<std::vector<int> > neighbor;
	bool found = false;
	bool targetFound = false;
	int randomOld;
	int randomNew;
	while(not found) {
		std::cout << "Me:" << std::endl;
		for(int i = 0; i < me.size(); i++) {
			std::cout << me[i][0] << " " << me[i][1] << " " << me[i][2] << std::endl;
		}
		randomOld = rand() % me.size();
		int x1 = me[randomOld][0];
		int y1 = me[randomOld][1];
		int z1 = me[randomOld][2];
		neighbor = this->field->neighbors(x1, y1, z1);
		while(neighbor.size() > 0) {
			std::cout << "Neighbors of " << x1 << " " << y1 << " " << z1 << ":" << std::endl;
			for(int i = 0; i < neighbor.size(); i++) {
				std::cout << neighbor[i][0] << " " << neighbor[i][1] << " "<< neighbor[i][2] << std::endl;
			}
			randomNew = rand() % neighbor.size();
			int x2 = neighbor[randomNew][0];
			int y2 = neighbor[randomNew][1];
			int z2 = neighbor[randomNew][2];
			if((*this->field)[x2][y2][z2] == EMPTY) {
				(*this->field)[x1][y1][z1] = EMPTY;
				(*this->field)[x2][y2][z2] = this->whoAmI();
				std::cout << "\nDone: ";
				std::cout << x1 << " " << y1 << " " << z1;
				std::cout << " --> ";
				std::cout << x2 << " " << y2 << " " << z2 << std::endl; 
				return;
			}
			else {
				neighbor.erase(neighbor.begin() + randomNew);
			}
		}
		me.erase(me.begin() + randomOld);
	}
	return;
}

void randomScreenAI::turnJump() {
	std::vector<std::vector<int> > me = this->field->allOfPlayer(
			this->whoAmI()
		);
	int random = rand() % me.size();
	int x1 = me[random][0];
	int y1 = me[random][1];
	int z1 = me[random][2];
	std::vector<std::vector<int> > empty = this->field->allOfPlayer(
			EMPTY
		);
	random = rand() % empty.size();
	int x2 = empty[random][0];
	int y2 = empty[random][1];
	int z2 = empty[random][2];
	(*this->field)[x1][y1][z1] = EMPTY;
	(*this->field)[x2][y2][z2] = this->whoAmI();
	return;
}

void randomScreenAI::turnWin() {
	exit(EXIT_SUCCESS);
	return;
}

void randomScreenAI::turnLose() {
	return;
}
