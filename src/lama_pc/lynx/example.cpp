/* 2010 Edward-Robert Tyercha
 * tyercha1@googlemail.com
 * edit: 2011
 */

#include "servoboard.h"
#include <iostream>
#include <stdlib.h>
#include <string>
#include "actionfield.h"

#include <iostream>
#include "serialline.h"

using namespace std;

typedef struct {
	float x;
	float y;
	float z;
	float z_above;
	float x_side;
	float y_side;
	float z_side;
	float rotation;
} BoardPosition;

const int POS_SLEEP = 27;
const int POS_NEW_TOKEN = 28;

void createBoardPosition(BoardPosition * pos, float x, float y, float z, float x_side, float y_side, float z_side, float rotation) {
	pos->x = x;
	pos->y = y;
	pos->z = z;
	pos->z_above = 3.0;
	pos->x_side = x_side;
	pos->y_side = y_side;
	pos->z_side = z_side;
	pos->rotation = rotation;
}

void moveToken(Actionfield* a, BoardPosition* posArray, int fromPos, int toPos) {
	
	// 1. Move above fromPos
	(*a).moveFingersToPos(posArray[fromPos].x,
			   posArray[fromPos].y,
			   posArray[fromPos].z_above,
			   1.0);
	// 1a. Rotate fingers and open
	(*(*a).getServoboard()).servo[5].setPos(posArray[fromPos].rotation);
	(*(*a).getServoboard()).updateSimultaneously(500);
	(*a).openFingers();
	// 2. Move to fromPos
	(*a).moveFingersToPos(posArray[fromPos].x,
			   posArray[fromPos].y,
			   posArray[fromPos].z,
			   1.0);
	(*a).closeFingers();
	sleep(1);
	// 3. Move to side position
	(*a).moveFingersToPos(posArray[fromPos].x_side,
			   posArray[fromPos].y_side,
			   posArray[fromPos].z_side,
			   1.0);
	// 4. Move above side position
	(*a).moveFingersToPos(posArray[fromPos].x_side,
			   posArray[fromPos].y_side,
			   posArray[fromPos].z_above,
			   1.0);
	// 5. Move above toPos
	(*a).moveFingersToPos(posArray[toPos].x,
			   posArray[toPos].y,
			   posArray[toPos].z_above,
			   1.0);
	// 5a. Rotate fingers
	(*(*a).getServoboard()).servo[5].setPos(posArray[toPos].rotation);
	(*(*a).getServoboard()).updateSimultaneously(500);
	// 6. Move to toPos
	(*a).moveFingersToPos(posArray[toPos].x,
			   posArray[toPos].y,
			   posArray[toPos].z,
			   1.0);
	(*a).openFingers();
	sleep(1);
	// 7. Move above toPos
	(*a).moveFingersToPos(posArray[toPos].x,
			   posArray[toPos].y,
			   posArray[toPos].z_above,
			   1.0);
}

void printHelp() {
	cout << "\nEingabe:\n";
	cout << "[q] Beenden\n";
	cout << "[1] bis [5] Gespeicherte Position ansteuern\n";
	cout << "[6] Digitale Eingänge\n";
	cout << "[7] Digitale Latch Eingänge\n";
	cout << "[8] Analoge Eingänge\n";
	cout << "[9] Digitaler einzelner Eingang\n";
	cout << "[m] dannach separat <x-pos> <y-pos> <z-pos> An Position Bewegen\n";
	cout << "[s] Servos-Stop\n";
	cout << "[x] Stein bewegen von <from-pos> zu <to-pos>\n";
	cout << "[r] Rotation (danach Wert zwischen 0 und 1)\n";
	cout << "[t] Test <start-pos> (bitte Stein auf Feld <start-pos> legen)\n";
	cout << endl;
}


int main() {

	// Create BoardPosition array
	BoardPosition positions[29];

	createBoardPosition(&positions[0], 5.5, 5.0, -2.0, 5.0, 7.5, -0.0, 0.1);
	createBoardPosition(&positions[1], 16, 3.0, -2.0, 19.0, 3.5, 0.0, 0.6);
	createBoardPosition(&positions[2], 25.0, 1.0, -2.0, 22.0, 2.5, 0.0, 0.1);
	createBoardPosition(&positions[3], 7.0, 14.0, -1.0, 5.0, 12.0, 0.0, 0.1);

	createBoardPosition(&positions[5], 27.0, 10.0, -1.5, 27.0, 8.0, 0.0, 0.2);
	createBoardPosition(&positions[6], 8.5, 22.5, -2.5, 8.5, 21.0, -1.0, 0.7);
	createBoardPosition(&positions[7], 19.0, 21.0, -2.0, 16.0, 20.0, -1.0, 0.6);
	createBoardPosition(&positions[8], 28.75, 18.5, -2.5, 28.5, 16.0, -1.0, 0.4);
	createBoardPosition(&positions[9], 9.5, 7.5, -2.0, 9.5, 10.0, -0.5, 0.4);
	createBoardPosition(&positions[10], 17.5, 6.0, -2.0, 20.0, 6.5, -0.5, 0.6);
	createBoardPosition(&positions[11], 23.5, 4.5, -2.0, 21.0, 4.0, -0.5, 0.6);
	createBoardPosition(&positions[12], 11.5, 13.25, -2.0, 11.5, 11.5, 0.0, 0.1);

	createBoardPosition(&positions[14], 24.5, 10.5, -2.0, 24.5, 9.0, -0.5, 0.2);
	createBoardPosition(&positions[15], 11.5, 20.0, -2.0, 13.0, 22.0, -0.5, 0.7);
	createBoardPosition(&positions[16], 19.5, 18.5, -2.0, 15.0, 18, -0.5, 0.6);
	createBoardPosition(&positions[17], 25.5, 17.5, -2.0, 26.0, 14.5, -0.5, 0.2);
	createBoardPosition(&positions[18], 14.0, 10.0, -2.0, 13.0, 8.0, -0.5, 0.75);
	createBoardPosition(&positions[19], 17.0, 9.0, -2.0, 17.0, 12.0, -0.5, 0.75);
	createBoardPosition(&positions[20], 21.0, 8.5, -2.0, 19.0, 11.0, -0.5, 0.45);
	createBoardPosition(&positions[21], 14.5, 13.0, -2.0, 18.0, 13.0, -0.5, 0.75);

	createBoardPosition(&positions[23], 21.5, 12.0, -2.0, 18.0, 12.0, -0.5, 0.4);
	createBoardPosition(&positions[24], 15.0, 16.0, -2.0, 16.5, 14.0, -0.5, 0.3);
	createBoardPosition(&positions[25], 18.5, 15.5, -2.0, 18.5, 13.0, -0.5, 0.3);
	createBoardPosition(&positions[26], 22.0, 15.0, -2.0, 22.0, 17.0, -0.5, 0.5);

	createBoardPosition(&positions[POS_SLEEP], 20.0, -6.5, -2.0, 20.0, -6.5, -2.0, 0.5);
	createBoardPosition(&positions[POS_NEW_TOKEN], 24.75, -6.5, -4.0, 22.0, -6.5, -4, 0.1);
	//~ /* extra zum testen*/ 
	//~ SerialLine s("/dev/ttyS0");
	//~ string str;
	//~ for( int i = 0; i < 2 ; i++ ) {	
		//~ str ="#0 P1500 #1 P1500 #2 P1500 #3 P1500 #4 P1500 #5 P1500 T2000 \n\r";
		//~ s.send((char*) str.c_str(), str.length());
		//~ cout << "::>"; int chars = 0;
		//~ while( s.dataWaiting() ) {
			//~ cout << s.readchar();
			//~ ++chars;
		//~ sleep(1);
	//~ } 
	//~ cout << "<:: numofchars:" << chars << endl;
    //~ }
	//~ exit(0);

	cout << "Baue Verbindung auf ... " << endl;
	Actionfield a("/dev/ttyUSB0"); // baut eine Verbindung mit 6 Servos auf.
	
	Servoboard& b = *(a.getServoboard()); // Vereinfachung
	if(!b.isConnected()) {
		exit(-1);
	}
	
	// muss "unbedingt" gemacht werden.
	a.resetArm();

	printHelp();
	string input;
	bool forever = true;
	bool digital[4];
	unsigned char analog[4];
	
	while(forever) {
		cin >> input;

		if(input == "1") {
			cout << "Bewegung 1" << endl;
			b.servo[0].setPos(0.5); 
			b.servo[1].setPos(0.5); 
			b.servo[2].setPos(0.5); 
			b.servo[3].setPos(0.5); 
			b.servo[4].setPos(0.5); 
			b.servo[5].setPos(0.5); 
			b.updateSimultaneously(2000); // innerhalb von 2 sekunden sollen
			                              // alle eingestellen positionen eingenommen werden
			                              
		} else if(input == "4") { // nur ein servo ansteuern  -- finger auf/zu
			cout << "Finger auf" << endl;
			//a.releaseAtPos(2.0,0.5);
			a.openFingers();

		} else if(input == "5") {
			cout << "Finger zu" << endl;
			//a.releaseAtPos(1.0,0.5);
			a.closeFingers();
			
		} else if(input == "2") {
			cout << "Bewegung 2" << endl;
			b.servo[0].setPos(0.370); 
			b.servo[1].setPos(0.377);
			b.servo[2].setPos(0.670); 
			b.servo[3].setPos(0.645); 
			b.servo[4].setPos(0.350);
			b.updateSimultaneously(2000);
			
		} else if(input == "3") {
			cout << "Bewegung 3" << endl;
			b.servo[0].setPos(0.550); 
			b.servo[1].setPos(0.450);
			b.servo[2].setPos(0.545); 
			b.servo[3].setPos(0.515); 
			b.servo[4].setPos(0.104);
			b.updateSimultaneously(2000);
		} else if(input == "6") {
			cout << b.readInputs(&(digital[0]),&(digital[1]),&(digital[2]),&(digital[3])) << " -> ";
			cout << "gelesen: A=" << (int)(digital[0]);
			cout << " B=" << (int)(digital[1]);
			cout << " C=" << (int)(digital[2]);
			cout << " D=" << (int)(digital[3]);
			cout << endl;
		} else if(input == "7") {
			cout <<  b.readLatchedInputs(&(digital[0]),&(digital[1]),&(digital[2]),&(digital[3])) << " -> ";
			cout << "gelesen: A=" << (int)(digital[0]);
			cout << " B=" << (int)(digital[1]);
			cout << " C=" << (int)(digital[2]);
			cout << " D=" << (int)(digital[3]);
			cout << endl;
		} else if(input == "8") {
			cout <<  b.readAnalogInputs(&(analog[0]),&(analog[1]),&(analog[2]),&(analog[3])) << " -> ";
			cout << "gelesen: A=" << (unsigned int)(analog[0]);
			cout << " B=" << (unsigned int)(analog[1]);
			cout << " C=" << (unsigned int)(analog[2]);
			cout << " D=" << (unsigned int)(analog[3]);
			cout << endl;
		} else if(input == "9") {
			cout << " Eingabe von Pin [A=0,B=1,C=2 oder D=3] " ;
			cin >> input;
			cout << " gelesen: " << b.getInput(atoi(input.c_str())) << endl;
			 
		} else if(input == "m") {
			cout << " Eingabe von x " ;
			cin >> input;
			float x = atof(input.c_str());
			cout << " Eingabe von y " ;
			cin >> input;
			float y = atof(input.c_str());
			cout << " Eingabe von z " ;
			cin >> input;
			float z = atof(input.c_str());
			cout << " Bewege zu " << x << " " << y << " " << z << endl ;
			a.moveFingersToPos(x,y,z,0.5);
		} else if(input == "x") {
			cout << " Von welcher Position?  " ;
			cin >> input;
			int fromPos = atoi(input.c_str());
			cout << " Wohin bewegen? " ;
			cin >> input;
			int toPos = atoi(input.c_str());
			moveToken(&a, positions, fromPos, toPos);
		} else if(input == "r") {
			cout << " Position zwischen 0 und 1?  " ;
			cin >> input;
			float pos = atof(input.c_str());
			b.servo[5].setPos(pos);
			b.updateSimultaneously(2000);
		} else if(input == "s") {
			cout << " Servo Stop " << endl;
			b.all_stop();
		} else if(input == "o") {
			cout << " Ausgabepin (=ch) 0 bis 31 (keine Servobelegung verwenden! <ch> <an=1/aus=0>" ;
			cin >> input;
			int ch = atoi(input.c_str());
			cin >> input;
			bool on = atoi(input.c_str()) == 1;
			cout << ch << " " << on << endl;
			b.setDiscreteOutput(ch,on);
		} else if(input == "t") {
			cout << " Von welcher Position?  " ;
			cin >> input;
			int fromPos = atoi(input.c_str());
			for (int j = fromPos; j < 27; j++) {
				if (j == 4 || j == 13 || j == 22) {
					continue;
				}
				int toPos = j + 1;
				if (j == 26) toPos = 0;
				if (j == 3 || j == 12 || j == 21) toPos = j + 2;
				moveToken(&a, positions, j, toPos);
			}
		} else if(input == "q") {
			forever = false;
		} else {
			
		}
		cout << " ... fertig " << endl;
		printHelp();
	}
	
	b.disconnect();
	
}
