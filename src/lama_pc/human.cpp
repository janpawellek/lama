#include"human.h"

Human::Human(boardinterface* board) {
	this->board = board;
}
Human::Human(boardinterface* board, Board* field)
	: Player(field) {
}

void Human::turn(phase moment) {
	std::cout << "Phase: " << playerPhase(moment) << std::endl;
	std::cout << "Du bist dran." << std::endl;
	switch(moment) {
		case SET:
			this->turnSet();
			break;
		case MOVE:
			this->turnMove();
			break;
		case JUMP:
			this->turnJump();
			break;
		case WIN:
			this->turnWin();
			break;
		case LOSE:
			this->turnLose();
			break;
		default:
			break;
	}
	this->field->print();
	return;
}

void Human::turnSet() {
	this->board->waitBoardNextChange();
	field = this->board->calculateRealBoard(
			field,
			this->whoAmI()
		);
	return;
}

void Human::turnMove() {
	this->board->waitBoardNextChange();
	this->board->waitBoardChange();
	field = this->board->calculateRealBoard(
			field,
			this->whoAmI()
		);
	return;
}

void Human::turnJump() {
	this->board->waitBoardNextChange();
	this->board->waitBoardChange();
	field = this->board->calculateRealBoard(
			field,
			this->whoAmI()
		);
	return;
}

void Human::turnWin() {
	std::cout << "GEWONNNEN" << std::endl;
	exit(EXIT_SUCCESS);
	return;
}

void Human::turnLose() {
	std::cout << "NEIIIN naja halt verloren" << std::endl;
	return;
}

void Human::beat() {
	std::cout << "Beat" << std::endl;
	this->board->waitBoardChange();
	field = this->board->calculateRealBoard(
			field,
			this->whoAmI()
		);
	return;
}


