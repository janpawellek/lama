#include"game.h"
#include<iostream>
#include<string>
#include<typeinfo>

game::play::play(Player * p, phase s, int t)
	: state(s),
		numTurns(t) {
			this->player = p;
}

game::game(Player * One, Player * Two)
	:	next(PLAYER_1),
		one(One, SET, 1),
		two(Two, SET, 1) {
	this->current = new Board();
	this->last = new Board(*current);
	One->me = PLAYER_1;
	Two->me = PLAYER_2;
	One->field = this->current;
	Two->field = this->current;
}

game::~game() {
	delete this->current;
	delete this->last;
}

// calls the turn function of the player
// whos turn it is
void game::nextTurn() {
	switch(this->next) {
		case PLAYER_1:
			/*
			this->one.player->turn(one.state);
			*/
			this->turn(this->one);
			this->one.numTurns += 1;
			this->next = PLAYER_2;
			break;
		case PLAYER_2:
			/*
			this->two.player->turn(two.state);
			*/
			this->turn(this->two);
			this->two.numTurns += 1;
			this->next = PLAYER_1;
			break;
		default:
			//some error handling
			break; // is an error if no statement is providet
	}
	return;
}

inline void game::confirme() {
	*this->last = *this->current;
}

inline void game::rewinde() {
	*this->current = *this->last;
}

void game::turn(game::play& p) {
	bool done = false;
	this->setNextPhase(p);
	while(not done) {
		p.player->turn(p.state);
		if(this->validTurn(p)) {
			bool beat = this->posibleBeat(p);
			this->confirme();
			if(beat) {
				bool beaten = false;
				while(not beaten) {
					p.player->beat();
					if(this->validBeat(p)) {
						this->confirme();
						beaten = true;
					}
					else {
						this->rewinde();
					}
				}
			}
			done = true;
			this->setNextPhase(p);
		}
		else {
			this->rewinde();
			std::cout << "Ungültiger Zug. Bitte Spielfeld wieder so anordnen:" << std::endl;
			this->current->print();
			std::cout << "Bitte Enter drücken, wenn fertig." << std::endl;
			std::string line;
			getline(std::cin, line);
		}
	}
}

// checks if a field is changed from
// the given player to empty
bool game::meToEmpty(int x, int y, int z, const play & me) const {
	if((*this->last)[x][y][z] == me.player->whoAmI()
			&& (*this->current)[x][y][z] == EMPTY
		) {
		return true;
	}
	return false;
}

// checks if a field is changed from empty
// to the given player
bool game::emptyToMe(int x, int y, int z, const play & me) const {
	if((*this->last)[x][y][z] == EMPTY
			&& (*this->current)[x][y][z] == me.player->whoAmI()
		) {
		return true;
	}
	return false;
}

// validates the turn
bool game::validTurn(const play& p) const {
	switch(p.state) {
		case SET:
			return validSet(p);
		case MOVE:
			return validMove(p);
		case JUMP:
			return validJump(p);
		case LOSE:
			return validLose(p);
		default:
			return false;
	}
}

// checks if one stone is set to an empty position
// doesn't work if a mill i closed
// and an enemy stone immediatly removed
bool game::validSet(const play& p) const {
	std::vector<std::vector<int> > change = this->diff();
	if(change.size() == 1) {
		int x = change[0][0];
		int y = change[0][1];
		int z = change[0][2];
		if((*this->last)[x][y][z] == EMPTY
				&& (*this->current)[x][y][z] == p.player->whoAmI()
			) {
			return true;
		}
	}
	return false;
}

// checks a standart move if its correckt
// a stone from the current player
// moved to empty neighboring field
bool game::validMove(const play& p) const {
	if(validJump(p)) {
		std::vector<std::vector<int> > change = this->diff();
		int x1 = change[0][0];
		int y1 = change[0][1];
		int z1 = change[0][2];
		int x2 = change[1][0];
		int y2 = change[1][1];
		int z2 = change[1][2];
		// if both are on a z axis line
		if((x1 == x2)
				&& (y1 == y2)
				&& ((x1 + y1)%2 == 1)
				&& ((z1 == (z2 - 1))
					|| (z1 == (z2 + 1))
					)
			) {
			return true;
		}
		// if both are on a y axis line
		if((x1 == x2)
				&& (z1 == z2)
				&& ((y1 == (y2 - 1))
					|| (y1 == (y2 + 1))
					)
			) {
			return true;
		}
		// if both are on ax axis line
		if((y1 == y2)
				&& (z1 == z2)
				&& ((x1 == (x2 - 1))
					|| (x1 == (x2 + 1))
					)
			) {
			return true;
		}
	}
	return false;
}

// checks if a stone of the given player
// is moved to a priviosly empty space
// is enogh to check turn in the JUMP phase
bool game::validJump(const play& p) const {
	std::vector<std::vector<int> > change = this->diff();
	if(change.size() == 2) {
		int x1 = change[0][0];
		int y1 = change[0][1];
		int z1 = change[0][2];
		int x2 = change[1][0];
		int y2 = change[1][1];
		int z2 = change[1][2];
		return (((*this->last)[x1][y1][z1] == EMPTY
				&& (*this->current)[x1][y1][z1] == p.player->whoAmI()
				&& (*this->last)[x2][y2][z2] == p.player->whoAmI()
				&& (*this->current)[x2][y2][z2] == EMPTY
			) || (
				(*this->current)[x1][y1][z1] == EMPTY
				&& (*this->last)[x1][y1][z1] == p.player->whoAmI()
				&& (*this->current)[x2][y2][z2] == p.player->whoAmI()
				&& (*this->last)[x2][y2][z2] == EMPTY
			)
		);
	}
	return false;
}

bool game::validLose(const play& p) const {
	std::vector<std::vector<int> > change = this->diff();
	if(change.size() == 0) {
		return true;
	}
	return false;
}

bool game::validWin(const play& p) const {
	std::vector<std::vector<int> > change = this->diff();
	if(change.size() == 0) {
		return true;
	}
	return false;
}

bool game::posibleBeat(const play& p) const {
	std::vector<std::vector<int> > change = this->diff();
	for(unsigned int i = 0; i < change.size(); i++) {
		int x = change[i][0];
		int y = change[i][1];
		int z = change[i][2];
		if(((*this->current)[x][y][z] == p.player->whoAmI())
				&& ((*this->current).isInMill(x, y, z))
		) {
			std::vector<std::vector<int> > enemyStones;
			enemyStones = this->current->allOfPlayer(
					this->current->enemyOf(
						p.player->whoAmI()
					)
				);
			for(unsigned int j = 0; j < enemyStones.size(); j++) {
				int x = enemyStones[j][0];
				int y = enemyStones[j][1];
				int z = enemyStones[j][2];
				if(not this->current->isInMill(x, y, z)) {
					return true;
				}
			}
			return false;
		}
	}
	return false;
}

bool game::validBeat(const play& p) const {
	std::vector<std::vector<int> > change = this->diff();;
	if(change.size() == 1) {
		int x = change[0][0];
		int y = change[0][1];
		int z = change[0][2];
		fieldstate enemy;
		switch(p.player->whoAmI()) {
			case PLAYER_1:
				enemy = PLAYER_2;
				break;
			case PLAYER_2:
				enemy = PLAYER_1;
				break;
			default:
				enemy = EMPTY;
		}
		return (((*this->current)[x][y][z] == EMPTY)
			&& ((*this->last)[x][y][z] ==  enemy)
			&& not ((*this->last).isInMill(x, y, z))
		);
	}
	return false;
}

// changes the phase of the
// given player
void game::setNextPhase(game::play& p) {

	// check if the enemy lose
	switch (p.player->whoAmI()) {
		case PLAYER_1:
			if (this->two.state == LOSE) {
				p.state = WIN;
				return;
			}
			break;
		case PLAYER_2:
			if (this->one.state == LOSE) {
				p.state = WIN;
				return;
			}
			break;
		default:
			std::cout << "Player nicht korrekt gesetzt" << std::endl;
	}

	// workes because it's imposible
	// to WIN (or LOSE) in the SET phase
	if(p.numTurns <= 9) {
		p.state = SET;
		return;
	}
	int enemyStones = this->countStones(this->current->enemyOf(p.player->whoAmI()));
	if(enemyStones < 3) {
		p.state = WIN;
		return;
	}
	int meStones = this->countStones(p.player->whoAmI());
	if(meStones == 3) {
		p.state = JUMP;
		return;
	}
	if(meStones < 3) {
		p.state = LOSE;
		return;
	}

	// zählen, ob freie Felder zum Bewegen da sind
	std::vector<std::vector<int> > allMe = this->current->allOfPlayer(p.player->whoAmI());
	int emptyNeighbors = 0;
	for (unsigned i = 0; i < allMe.size(); i++) {
		int x = allMe[i][0];
		int y = allMe[i][1];
		int z = allMe[i][2];
		std::vector<std::vector<int> > neighbors = this->current->neighbors(x, y, z);
		for (unsigned j = 0; j < neighbors.size(); j++) {
			int x2 = neighbors[j][0];
			int y2 = neighbors[j][1];
			int z2 = neighbors[j][2];
			if ((*this->current)[x2][y2][z2] == EMPTY) {
				emptyNeighbors++;
			}
		}
	}
	if (emptyNeighbors == 0) {
		p.state = LOSE;
		return;
	}

	p.state = MOVE;
	return;
}

// gives an array of all coordinates
// on which hase been change
std::vector<std::vector<int> > game::diff() const {
	std::vector<std::vector<int> > diff;
	for(int x = 0; x < 3; x++) {
		for(int y = 0; y < 3; y++) {
			for(int z = 0; z < 3; z++) {
				if((*this->current)[x][y][z] != (*this->last)[x][y][z]) {
					diff.push_back({x, y, z});
				}
			}
		}
	}
	return diff;
}

int game::countStones(fieldstate player) const {
	int count = 0;
	for(int x = 0; x < 3; x++) {
		for(int y = 0; y < 3; y++) {
			for(int z = 0; z < 3; z++) {
				count += ((*this->current)[x][y][z] == player ? 1 : 0);
			}
		}
	}
	return count;
}
