include(qextserial/src/qextserialport.pri)

QMAKE_CXXFLAGS += -std=c++0x

HEADERS += arminterface.h
HEADERS += boardinterface.h
HEADERS += game.h
HEADERS += board.h
HEADERS += human.h
HEADERS += player.h
HEADERS += randomscreenai.h
HEADERS += screenhuman.h
HEADERS += randomaiarm.h
HEADERS += freedomaiarm.h

HEADERS += lynx/actionfield.h
HEADERS += lynx/serialline.h
HEADERS += lynx/servoboard.h
HEADERS += lynx/servo.h

SOURCES += lynx/actionfield.cpp
SOURCES += lynx/serialline.cpp
SOURCES += lynx/servoboard.cpp
SOURCES += lynx/servo.cpp

SOURCES += arminterface.cpp
SOURCES += board.cpp
SOURCES += boardinterface.cpp
SOURCES += game.cpp
SOURCES += human.cpp
SOURCES += lama.cpp
SOURCES += player.cpp
SOURCES += randomaiarm.cpp
SOURCES += randomscreenai.cpp
SOURCES += screenhuman.cpp
SOURCES += freedomaiarm.cpp
