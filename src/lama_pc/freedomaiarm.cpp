#include"freedomaiarm.h"

freedomAIArm::freedomAIArm(
		arminterface * a,
		boardinterface * b
	)
	: Player() {
	this->arm = a;
	this->board = b;
}

freedomAIArm::freedomAIArm(
		arminterface * a,
		boardinterface * b,
		Board * f
	)
	: Player(f) {
	this->arm = a;
	this->board = b;
}

void freedomAIArm::turn(phase moment) {
	std::cout << "Phase: " << playerPhase(moment) << std::endl;
	std::cout << "Ich bin dran." << std::endl;
	srand(time(NULL));
	switch(moment) {
		case SET:
			this->turnSet();
			break;
		case MOVE:
			this->turnMove();
			break;
		case JUMP:
			this->turnJump();
			break;
		case WIN:
			this->turnWin();
		
	break;
		case LOSE:
			this->turnLose();
			break;
		default:
			break;
	}
	this->field->print();
	return;
}

int freedomAIArm::freedom(int x, int y, int z) {
	int freedoms = 0;

	vector<vector<int> > neighbors = this->field->neighbors(x, y, z);
	for (unsigned int i = 0; i < neighbors.size(); i++) {
		int x = neighbors[i][0];
		int y = neighbors[i][1];
		int z = neighbors[i][2];
		if ((*this->field)[x][y][z] == this->field->enemyOf(this->whoAmI())) {
			freedoms++;
		}
	}
	return freedoms;
}

void freedomAIArm::beat() {
	std::cout << "Yeah! Ich habe eine Mühle gemacht!" << std::endl;
	vector<std::vector<int> > enemy = this->field->allOfPlayer(
			this->field->enemyOf(
				this->whoAmI()
			)
		);
	vector<vector<int> > bestbeatable;
	int maxfreedom = 0;
	for (unsigned int i = 0; i < enemy.size(); i++) {
		int x = enemy[i][0];
		int y = enemy[i][1];
		int z = enemy[i][2];
		if(not this->field->isInMill(x, y, z)) {
			int curfreedom = this->freedom(x, y, z);
			if (curfreedom == maxfreedom) {
				bestbeatable.push_back({x, y, z});
				continue;
			}
			if (curfreedom > maxfreedom) {
				maxfreedom = curfreedom;
				bestbeatable.clear();
				bestbeatable.push_back({x, y, z});
				continue;
			}
		}
	}

	int random = rand() % bestbeatable.size();
	// remove stone from position
	// by moving arm herer
	int x = bestbeatable[random][0];
	int y = bestbeatable[random][1];
	int z = bestbeatable[random][2];
	arm->moveToken(arm->convertToArmPos(x, y, z), POS_SLEEP);
	this->board->getBoardStatus();
	arm->moveToPos(POS_SLEEP);
	this->field = this->board->calculateRealBoard(
			this->field,
			this->whoAmI()
		);
	return;
}

void freedomAIArm::turnSet() {
	std::vector<std::vector<int> > empty = this->field->allOfPlayer(
			EMPTY
		);

	// find max. freedom value
	int maxfreedom = 0;
	for (unsigned int i = 0; i < empty.size(); i++) {
		int curfreedom = this->freedom(empty[i][0], empty[i][1], empty[i][2]);
		if (curfreedom > maxfreedom) {
			maxfreedom = curfreedom;
		}
	}

	// find all positions with max. freedom value
	vector<vector<int> > maxempty;
	for (unsigned int i = 0; i < empty.size(); i++) {
		int curfreedom = this->freedom(empty[i][0], empty[i][1], empty[i][2]);
		if (curfreedom == maxfreedom) {
			maxempty.push_back(empty[i]);
		}
	}

	int random = rand() % maxempty.size();
	int x = maxempty[random][0];
	int y = maxempty[random][1];
	int z = maxempty[random][2];
	// set on field x y z
	// by using the arm
	arm->moveToken(POS_NEW_TOKEN, arm->convertToArmPos(x, y, z));
	this->board->getBoardStatus();
	arm->moveToPos(POS_SLEEP);
	this->field = this->board->calculateRealBoard(
			this->field,
			this->whoAmI()
		);
	return;
}

void freedomAIArm::turnMove() {
	std::vector<std::vector<int> > me = this->field->allOfPlayer(
			this->whoAmI()
		);
	vector<Move> bestmoves;
	int bestfreedom = -4;
	for (unsigned int i = 0; i < me.size(); i++) {
		int x1 = me[i][0];
		int y1 = me[i][1];
		int z1 = me[i][2];
		int blocking_freedom = this->freedom(x1, y1, z1);

		std::vector<std::vector<int> > neighbors;
		neighbors = this->field->neighbors(x1, y1, z1);
		for (unsigned int j = 0; j < neighbors.size(); j++) {
			int x2 = neighbors[j][0];
			int y2 = neighbors[j][1];
			int z2 = neighbors[j][2];

			if((*this->field)[x2][y2][z2] == EMPTY) {
				int new_freedom = this->freedom(x2, y2, z2);

				Move curmove;
				curmove.x1 = x1;
				curmove.y1 = y1;
				curmove.z1 = z1;
				curmove.x2 = x2;
				curmove.y2 = y2;
				curmove.z2 = z2;

				int freedom_diff = new_freedom - blocking_freedom;
				if (freedom_diff == bestfreedom) {
					bestmoves.push_back(curmove);
					continue;
				}
				if (freedom_diff > bestfreedom) {
					bestfreedom = freedom_diff;
					bestmoves.clear();
					bestmoves.push_back(curmove);
					continue;
				}
			}
		}
	}
	
	int random = rand() % bestmoves.size();
	// take from x1 y1 z1 
	// with the arm
	// move to x2 y2 z2
	arm->moveToken(arm->convertToArmPos(bestmoves[random].x1, bestmoves[random].y1, bestmoves[random].z1),
		arm->convertToArmPos(bestmoves[random].x2, bestmoves[random].y2, bestmoves[random].z2));
	// get the changed board
	this->board->getBoardStatus();
	arm->moveToPos(POS_SLEEP);
	this->field = this->board->calculateRealBoard(
			this->field,
			this->whoAmI()
		);
	return;
}

void freedomAIArm::turnJump() {
	std::vector<std::vector<int> > me = this->field->allOfPlayer(
			this->whoAmI()
		);
	int maxfreedom = 4;
	vector<vector<int> > besttokens;
	for (unsigned int i = 0; i < me.size(); i++) {
		int x1 = me[i][0];
		int y1 = me[i][1];
		int z1 = me[i][2];
		int curfreedom = this->freedom(x1, y1, z1);

		if (curfreedom == maxfreedom) {
			besttokens.push_back({x1, y1, z1});
			continue;
		}
		if (curfreedom < maxfreedom) {
			maxfreedom = curfreedom;
			besttokens.clear();
			besttokens.push_back({x1, y1, z1});
		}
	}
		
	int random = rand() % besttokens.size();
	int x1 = besttokens[random][0];
	int y1 = besttokens[random][1];
	int z1 = besttokens[random][2];
	std::vector<std::vector<int> > empty = this->field->allOfPlayer(
			EMPTY
		);
	maxfreedom = 0;
	vector<vector<int> > bestempty;
	for (unsigned int j = 0; j < empty.size(); j++) {
		int x1 = empty[j][0];
		int y1 = empty[j][1];
		int z1 = empty[j][2];
		int curfreedom = this->freedom(x1, y1, z1);

		if (curfreedom == maxfreedom) {
			bestempty.push_back({x1, y1, z1});
			continue;
		}
		if (curfreedom > maxfreedom) {
			maxfreedom = curfreedom;
			bestempty.clear();
			bestempty.push_back({x1, y1, z1});
		}
	}
	random = rand() % bestempty.size();
	int x2 = bestempty[random][0];
	int y2 = bestempty[random][1];
	int z2 = bestempty[random][2];
	// take from x1 y1 z1 
	// move to x2 y2 z2
	// with the arm
	arm->moveToken(arm->convertToArmPos(x1, y1, z1),
		arm->convertToArmPos(x2, y2, z2));
	// get the changed board
	this->board->getBoardStatus();
	arm->moveToPos(POS_SLEEP);
	this->field = this->board->calculateRealBoard(
			this->field,
			this->whoAmI()
		);
	return;
}

void freedomAIArm::turnWin() {
	exit(EXIT_SUCCESS);
	return;
}

void freedomAIArm::turnLose() {
	return;
}
