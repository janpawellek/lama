
#include"boardinterface.h"

using namespace std;

boardinterface::boardinterface() {
	this->port = new QextSerialPort;
	this->board = new Board;
	//connected = false;
}

boardinterface::~boardinterface() {
	disconnect();
}

bool boardinterface::connect(char* device) {
	if(not (this->port->isOpen())) {
		port->setPortName((QString)device);
		port->setBaudRate(BAUD9600);
		port->setDataBits(DATA_8);
		port->setParity(PAR_NONE);
		port->setStopBits(STOP_1);
		port->setFlowControl(FLOW_OFF);
		port->open(QIODevice::ReadWrite);
		return true;
	}
	return false;
}

bool boardinterface::disconnect() {
	if(this->port->isOpen()) {
		this->port->close();
		return true;
	}
	return false;
}

int boardinterface::charToNum(int number, char c) {
	number *= 10;
	switch(c) {
		case '0':
			return number;
		case '1':
			return number + 1;
		case '2':
			return number + 2;
		case '3':
			return number + 3;
		case '4':
			return number + 4;
		case '5':
			return number + 5;
		case '6':
			return number + 6;
		case '7':
			return number + 7;
		case '8':
			return number + 8;
		case '9':
			return number + 9;
		default:
			return number / 10;
	}
}

Board * boardinterface::readBoard() {
	if(not this->port->isOpen()) {
		return NULL;
	}
	int length;
	bool lineComplete = false;
	int size = 16;
	char buffer[size];
	bool negative = false;
	int number = 0;
	while(not lineComplete) {
		this->port->waitForReadyRead(-1);
		length = this->port->read(buffer, size);
		if(length < 0) {
			std::cerr << "can't read anything from device";
		}
		if(length > 0) {
			for (int k = 0; k < length; k++) {
				int x, y, z;
				switch (buffer[k]) {
					case '-':
						negative = true;
						break;
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						number = this->charToNum(number, buffer[k]);
						break;
					case ' ':
						// Zahl ist jetzt fertig eingelesen
						x = getPcCoordX(number);
						y = getPcCoordY(number);
						z = getPcCoordZ(number);
						if (negative) {
							// Feld leer
							(*this->board)[x][y][z] = EMPTY;
						}
						else {
							(*this->board)[x][y][z] = PLAYER_UNKNOWN;
						}
						negative = false;
						number = 0;
						break;
					case '\r':
						break;
					case '\n':
						lineComplete = true;
						break;
					default:
						std::cerr << "Unbekanntes Zeichen: ";
						std::cerr << buffer[k];
						negative = false;
						number = 0;
						break;
				}
			}
		}
	}
	return this->board;
}

Board * boardinterface::waitBoardChange() {
	if (not this->port->isWritable()) {
		std::cerr << "can't send command to board";
		return NULL;
	}
	if(this->port->putChar('w')) {
		return this->readBoard();
	}
	else {
		return NULL;
	}
}

Board * boardinterface::waitBoardNextChange() {
	if (not this->port->isWritable()) {
		std::cerr << "can't send command to board";
		return NULL;
	}
	if(this->port->putChar('n')) {
		return this->readBoard();
	}
	else {
		return NULL;
	}
}

Board * boardinterface::getBoardStatus() {
	if (not this->port->isWritable()) {
		std::cerr << "can't send command to board";
		return NULL;
	}
	if(this->port->putChar('s')) {
		return this->readBoard();
	}
	else {
		return NULL;
	}
}

Board * boardinterface::calculateRealBoard(
		Board * status,
		fieldstate player
	) {
	for(int x = 0; x < 3; x++) {
		for(int y = 0; y < 3; y++) {
			for(int z = 0; z < 3; z++) {
				if(x == 1 && y == 1) {
					(*this->board)[x][y][z] = INVALID_FIELD;
					(*status)[x][y][z] = INVALID_FIELD;
					continue;
				}
				switch((*this->board)[x][y][z]) {
					case EMPTY:
						(*status)[x][y][z] = EMPTY;
						break;
					case PLAYER_UNKNOWN:
						if((*status)[x][y][z] == PLAYER_1
								or (*status)[x][y][z] == PLAYER_2
							) {
							continue;
						}
						else {
							(*status)[x][y][z] = player;
						}
						break;
					default:
						continue;
				}
			}
		}
	}
	return status;
}

int boardinterface::getPcCoordX(int mcCoord) {
	mcCoord -= 1;
	return mcCoord % 3;
}

int boardinterface::getPcCoordY(int mcCoord) {
	mcCoord -= 1;
	return ((mcCoord - getPcCoordX(mcCoord + 1)) / 3) % 3;
}

int boardinterface::getPcCoordZ(int mcCoord) {
	mcCoord -= 1;
	return (mcCoord - (getPcCoordY(mcCoord + 1) * 3)) / 9;
}
