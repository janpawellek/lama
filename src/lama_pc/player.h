#ifndef PLAYER_H
#define PLAYER_H

#include"board.h"

enum phase {
	SET  = 0, // start of the Game 
	MOVE = 1, // after all stones are placed
	JUMP = 2, // if player has only 3 stones left
	WIN  = 3, // if the player has won
	LOSE = 4, // if the player has lost
};

class Player {
	friend class game;
	private:
		fieldstate me;

	protected:
		Board * field;

	public:
		Player();
		Player(Board*);
		//Player(Board&, fieldstate);

		//virtual void turn() =0;
		virtual void turn(phase) =0;
		virtual void beat() =0;
		const fieldstate& whoAmI() const;
		std::string playerPhase(phase& p);
};

#endif //PLAYER_H
