#ifndef SCREENHUMAN_H
#define SCREENHUMAN_H

#include"player.h"

class screenhuman : public Player {
	private:
		// checks to get a valid move
		// getMove();
		void turnSet();
		void turnMove();
		void turnJump();
		void turnWin();
		void turnLose();

	public:
		screenhuman();
		screenhuman(Board *);
		//void turn();
		void turn(phase);
		void beat();

		void print();
};

#endif //SCREENHUMAN_H
