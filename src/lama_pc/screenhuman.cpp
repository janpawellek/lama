#include"screenhuman.h"

#include<iostream>
#include<string>
#include<unistd.h>

using namespace std;

screenhuman::screenhuman()
	: Player() {
}

screenhuman::screenhuman(Board * f)
	: Player(f) {
}

/*
string fieldToStr(fieldstate field) {
	switch(field) {
		case EMPTY:
			return "+";
		case PLAYER_1:
			return "\033[0;31m@\033[0m";
		case PLAYER_2:
			return "\033[0;32mX\033[0m";
		case INVALID_FIELD:
		default:
			return " ";
	}
}
*/

string playerPhase(phase& p) {
	switch(p) {
		case SET:
			return "Setzen";
		case MOVE:
			return "Bewegen";
		case JUMP:
			return "Springen";
		case WIN:
			return "Du hast gewonnen";
		case LOSE:
			return "Du hast verloren";
	}
	return "Irgendetwas ging irgendwo furchtbar schief";
}

void screenhuman::print() {
	//this->field->print();
	
	cout << "Spieler: " << fieldToStr(this->whoAmI()) << endl;
	
	cout << "  " << fieldToStr((*this->field)[0][2][0]) << "---------" << fieldToStr((*this->field)[1][2][0]) << "---------" << fieldToStr((*this->field)[2][2][0]) << endl;
	cout << "  |         |         |" << endl;
	cout << "  |  " << fieldToStr((*this->field)[0][2][1]) << "------" << fieldToStr((*this->field)[1][2][1]) << "------" << fieldToStr((*this->field)[2][2][1]) << "  |" << endl;
	cout << "  |  |      |      |  |" << endl;
	cout << "  |  |  " << fieldToStr((*this->field)[0][2][2]) << "---" << fieldToStr((*this->field)[1][2][2]) << "---" << fieldToStr((*this->field)[2][2][2]) << "  |  |" << endl;
	cout << "  |  |  |       |  |  |" << endl;
	cout << "  " << fieldToStr((*this->field)[0][1][0]) << "--" << fieldToStr((*this->field)[0][1][1]) << "--" << fieldToStr((*this->field)[0][1][2]) << "       " << fieldToStr((*this->field)[2][1][2]) << "--" << fieldToStr((*this->field)[2][1][1]) << "--" << fieldToStr((*this->field)[2][1][0]) << endl;
	cout << "  |  |  |       |  |  |" << endl;
	cout << "  |  |  " << fieldToStr((*this->field)[0][0][2]) << "---"<< fieldToStr((*this->field)[1][0][2]) <<  "---" << fieldToStr((*this->field)[2][0][2]) << "  |  |" << endl;
	cout << "  |  |      |      |  |" << endl;
	cout << "  |  " << fieldToStr((*this->field)[0][0][1]) << "------" << fieldToStr((*this->field)[1][0][1]) << "------" << fieldToStr((*this->field)[2][0][1]) << "  |" << endl;
	cout << "  |         |         |" << endl;
	cout << "  " << fieldToStr((*this->field)[0][0][0]) << "---------" << fieldToStr((*this->field)[1][0][0]) << "---------" << fieldToStr((*this->field)[2][0][0]) << endl;
	cout << endl;
}

void screenhuman::turn(phase moment) {
	cout << playerPhase(moment) << endl;
	switch(moment) {
		case SET:
			this->turnSet();
			break;
		case MOVE:
			this->turnMove();
			break;
		case JUMP:
			this->turnJump();
			break;
		case WIN:
			this->turnWin();
			break;
		case LOSE:
			this->turnLose();
			break;
		default:
			break;
	}
	return;
}

void screenhuman::beat() {
	int x;
	int y;
	int z;
	cout << "Schlagen" << endl;
	this->print();
	cout << "X, Y, Z werte eingeben: ";
	cin >> x;
	cin >> y;
	cin >> z;
	if((*this->field)[x][y][z] == this->field->enemyOf(this->whoAmI())
			&& not this->field->isInMill(x, y, z)
		) {
		(*this->field)[x][y][z] = EMPTY;
	}
	else {
		cout << "Nicht moeglich, Versuchs nochmal" << endl;
		this->beat();
	}
	return;
}

void screenhuman::turnSet() {
	int x;
	int y;
	int z;
	this->print();
	cout << "X, Y, Z werte eingeben: ";
	cin >> x;
	cin >> y;
	cin >> z;
	if(
			x >= 0 && y >= 0 && z >= 0
			&& x < 3 && y < 3 && z < 3
			&& (*this->field)[x][y][z] == EMPTY
		) {
		(*this->field)[x][y][z] = this->whoAmI();
	}
	else {
		cout << "Nicht moeglich, Versuchs nochmal" << endl;
		this->turnSet();
	}
	return;
}

void screenhuman::turnMove() {
	int x1;
	int y1;
	int z1;
	this->print();
	cout << "X, Y, Z werte eingeben: ";
	cin >> x1;
	cin >> y1;
	cin >> z1;
	if((*this->field)[x1][y1][z1] == this->whoAmI()) {
		int x2;
		int y2;
		int z2;
		cout << "Ziel Feld angeben (X, Y, Z); ";
		cin >> x2;
		cin >> y2;
		cin >> z2;
		if((*this->field)[x2][y2][z2] == EMPTY
				&& this->field->isNeighbor(x1, y1, z1, x2, y2, z2)
		) {
			(*this->field)[x1][y1][z1] = EMPTY;
			(*this->field)[x2][y2][z2] = this->whoAmI();
		}
		else {
			cout << "Das Klappt so nicht" << endl;
			cout << "Versuchs nochmal." << endl;
			this->turnMove();
		}
	}
	else {
		cout << "Du kannst nur eigene steine bewegen." << endl;
		cout << "Versuchs nochmal." << endl;
		this->turnMove();
	}
	return;
}

void screenhuman::turnJump() {
	int x1;
	int y1;
	int z1;
	this->print();
	cout << "X, Y, Z werte eingeben: ";
	cin >> x1;
	cin >> y1;
	cin >> z1;
	if((*this->field)[x1][y1][z1] == this->whoAmI()) {
		int x2;
		int y2;
		int z2;
		cout << "Ziel Feld angeben (X, Y, Z); ";
		cin >> x2;
		cin >> y2;
		cin >> z2;
		if((*this->field)[x2][y2][z2] == EMPTY) {
			(*this->field)[x1][y1][z1] = EMPTY;
			(*this->field)[x2][y2][z2] = this->whoAmI();
		}
		else {
			this->turnJump();
		}
	}
	else {
		cout << "Du kannst nur eigene steine bewegen." << endl;
		cout << "Versuchs nochmal." << endl;
		this->turnMove();
	}
	return;
}

void screenhuman::turnWin() {
	this->print();
	cout << "Gratulation und so!" << endl;
	exit(EXIT_SUCCESS);
	return;
}

void screenhuman::turnLose() {
	this->print();
	cout << "Na das war woll nichts" << endl;
	return;
}
