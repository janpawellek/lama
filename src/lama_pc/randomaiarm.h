#ifndef RANDOMAIARM_H
#define RANDOMAIARM_H

#include"player.h"
#include"boardinterface.h"
#include"arminterface.h"

#include<iostream>
#include<time.h>
#include<stdlib.h>
#include"arminterface.h"
#include"boardinterface.h"

class randomAIArm : public Player {
	private:
		arminterface * arm;
		boardinterface * board;

		void turnSet();
		void turnMove();
		void turnJump();
		void turnWin();
		void turnLose();

	public:
		randomAIArm(
				arminterface*,
				boardinterface*
			);
		randomAIArm(
				arminterface*,
				boardinterface*,
				Board*
			);

		void turn(phase);
		void beat();
};

#endif //RANDOMAIARM_H
