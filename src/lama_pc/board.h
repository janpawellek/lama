#ifndef BOARD_H
#define BOARD_H

#include <string>
#include <iostream>
#include <vector>

enum fieldstate {
	EMPTY = 0,
	PLAYER_1 = 1,
	PLAYER_2 = 2,
	INVALID_FIELD = 3,
	PLAYER_UNKNOWN = 4,
};

std::string fieldToStr(fieldstate);

class Board {
	private:
		fieldstate* field;

	public:
		Board();
		Board(const Board&);
		~Board();
		
		Board& operator=(const Board&);

		fieldstate& getFieldAt(int, int, int);

		// returns all coordinates occupied by a given player
		std::vector<std::vector<int> > allOfPlayer(fieldstate);
		// returns all neighboring fields to a coordinate
		std::vector<std::vector<int> > neighbors(int, int, int);
		// checks if a gieven field is part of a Mill
		bool isInMill(int, int, int);

		void print();

		fieldstate enemyOf(fieldstate p);

		bool isNeighbor(
				int, int, int,
				int, int, int
			) const;

		class XProxy {
			private:
				fieldstate* field;
				int relPos;

			public:
				XProxy(fieldstate*, int);

				class YProxy {
					private:
						fieldstate* field;
						int relPos;

					public:
						YProxy(fieldstate*, int);

						fieldstate& operator[](int);
				};
				YProxy operator[](int);
		};
		XProxy operator[](int);
};

#endif //BOARD_H
