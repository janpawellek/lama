#ifndef HUMAN_H
#define HUMAN_H

#include"player.h"
#include"boardinterface.h"

class Human : public Player {
	private:
		void turnSet();
		void turnMove();
		void turnJump();
		void turnWin();
		void turnLose();

		boardinterface * board;

	public:
		Human(boardinterface*);
		Human(boardinterface*, Board*);
		void turn(phase);
		void beat();
};


#endif //HUMAN_H
