#ifndef FREEDOMAIARM_H
#define FREEDOMAIARM_H

#include"player.h"
#include"boardinterface.h"
#include"arminterface.h"

#include<iostream>
#include<time.h>
#include<stdlib.h>
#include"arminterface.h"
#include"boardinterface.h"

class freedomAIArm : public Player {
	private:
		arminterface * arm;
		boardinterface * board;

		int freedom(int x, int y, int z);

		void turnSet();
		void turnMove();
		void turnJump();
		void turnWin();
		void turnLose();

	public:
		freedomAIArm(
				arminterface*,
				boardinterface*
			);
		freedomAIArm(
				arminterface*,
				boardinterface*,
				Board*
			);

		void turn(phase);
		void beat();
};

struct Move {
	int x1;
	int y1;
	int z1;
	int x2;
	int y2;
	int z2;
};

#endif //FREEDOMAIARM_H
