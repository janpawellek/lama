#include "board.h"
#include <iostream>
#include <string>

using namespace std;

string fieldToStr(fieldstate field) {
	switch(field) {
		case EMPTY:
			return "+";
		case PLAYER_1:
			return "\033[0;31m@\033[0m";
		case PLAYER_2:
			return "\033[0;32mX\033[0m";
		case PLAYER_UNKNOWN:
			return "\033[0;33m?\033[0m";
		case INVALID_FIELD:
		default:
				return " ";
	}
}

void Board::print() {
	cout << endl;
	cout << "  " << fieldToStr((*this)[0][2][0]) << "---------" << fieldToStr((*this)[1][2][0]) << "---------" << fieldToStr((*this)[2][2][0]) << endl;
	cout << "  |         |         |" << endl;
	cout << "  |  " << fieldToStr((*this)[0][2][1]) << "------" << fieldToStr((*this)[1][2][1]) << "------" << fieldToStr((*this)[2][2][1]) << "  |" << endl;
	cout << "  |  |      |      |  |" << endl;
	cout << "  |  |  " << fieldToStr((*this)[0][2][2]) << "---" << fieldToStr((*this)[1][2][2]) << "---" << fieldToStr((*this)[2][2][2]) << "  |  |" << endl;
	cout << "  |  |  |       |  |  |" << endl;
	cout << "  " << fieldToStr((*this)[0][1][0]) << "--" << fieldToStr((*this)[0][1][1]) << "--" << fieldToStr((*this)[0][1][2]) << "       " << fieldToStr((*this)[2][1][2]) << "--" << fieldToStr((*this)[2][1][1]) << "--" << fieldToStr((*this)[2][1][0]) << endl;
	cout << "  |  |  |       |  |  |" << endl;
	cout << "  |  |  " << fieldToStr((*this)[0][0][2]) << "---"<< fieldToStr((*this)[1][0][2]) <<  "---" << fieldToStr((*this)[2][0][2]) << "  |  |" << endl;
	cout << "  |  |      |      |  |" << endl;
	cout << "  |  " << fieldToStr((*this)[0][0][1]) << "------" << fieldToStr((*this)[1][0][1]) << "------" << fieldToStr((*this)[2][0][1]) << "  |" << endl;
	cout << "  |         |         |" << endl;
	cout << "  " << fieldToStr((*this)[0][0][0]) << "---------" << fieldToStr((*this)[1][0][0]) << "---------" << fieldToStr((*this)[2][0][0]) << endl;
	cout << endl;
}

fieldstate Board::enemyOf(fieldstate p) {
	switch(p) {
		case PLAYER_1:
			return PLAYER_2;
		case PLAYER_2:
			return PLAYER_1;
		default:
			return EMPTY;
	}
	return INVALID_FIELD;
}

Board::Board() {
	this->field = new fieldstate[27];
	for(int i = 0; i < 27; i++) {
		switch(i) {
			case 4:
			case 13:
			case 22:
				this->field[i] = INVALID_FIELD;
				break;
			default:
				this->field[i] = EMPTY;
		}
	}
}

Board::Board(const Board& other) {
	this->field = new fieldstate[27];
	for(int i = 0; i < 27; i++) {
		this->field[i] = other.field[i];
	}
}

Board::~Board() {
	delete[] this->field;
}

Board& Board::operator=(const Board& other) {
	if(this != &other) {
		for(int i = 0; i < 27; i++) {
			this->field[i] = other.field[i];
		}
	}
	return *this;
}

fieldstate& Board::getFieldAt(int x, int y, int z) {
	return this->field[x + y*3 + z*9];
}

std::vector<std::vector<int> > Board::allOfPlayer(fieldstate p) {
	std::vector<std::vector<int> > list;
	for(int x = 0; x < 3; x++) {
		for(int y = 0; y < 3; y++) {
			for(int z = 0; z < 3; z++) {
				if((*this)[x][y][z] == p) {
					list.push_back({x, y, z});
				}
			}
		}
	}
	return list;
}

std::vector<std::vector<int> > Board::neighbors(int x, int y, int z) {
	std::vector<std::vector<int> > neighborhood;
	if(this->isNeighbor(x, y, z, x + 1, y, z)) {
		neighborhood.push_back({x + 1, y, z});
	}
	if(this->isNeighbor(x, y, z, x - 1, y, z)) {
		neighborhood.push_back({x - 1, y, z});
	}
	if(this->isNeighbor(x, y, z, x, y + 1, z)) {
		neighborhood.push_back({x, y + 1, z});
	}
	if(this->isNeighbor(x, y, z, x, y - 1, z)) {
		neighborhood.push_back({x, y - 1, z});
	}
	if(this->isNeighbor(x, y, z, x, y, z + 1)) {
		neighborhood.push_back({x, y, z + 1});
	}
	if(this->isNeighbor(x, y, z, x, y, z - 1)) {
		neighborhood.push_back({x, y, z - 1});
	}
	return neighborhood;
}

bool Board::isInMill(int x, int y, int z) {
	int line = 0;
	if((x + y)%2 == 1) {
		for(int i = 0; i < 3; i++) {
			if((*this)[x][y][z] == (*this)[x][y][i]) {
				line += 1;
			}
		}
		if(line == 3) {
			return true;
		}
		line = 0;
	}
	for(int i = 0; i < 3; i++) {
		if((*this)[x][y][z] == (*this)[x][i][z]) {
			line += 1;
		}
	}
	if(line == 3) {
		return true;
	}
	line = 0;
	for(int i = 0; i < 3; i++) {
		if((*this)[x][y][z] == (*this)[i][y][z]) {
			line += 1;
		}
	}
	if(line == 3) {
		return true;
	}
	return false;
}

Board::XProxy::XProxy(fieldstate* _field, int x):
	field(_field), relPos(x) {
}

Board::XProxy::YProxy::YProxy(fieldstate* _field, int y):
	field(_field), relPos(y) {
}

Board::XProxy Board::operator[](int x) {
	return XProxy(this->field, x);
}

Board::XProxy::YProxy Board::XProxy::operator[](int y) {
	y *= 3;
	y += this->relPos;
	return YProxy(this->field, y);
}

fieldstate& Board::XProxy::YProxy::operator[](int z) {
	z *= 9;
	z += this->relPos;
	return this->field[z];
}

bool Board::isNeighbor(
		int x1, int y1, int z1,
		int x2, int y2, int z2
	) const {
	if(x2 < 0 || y2 < 0 || z2 < 0
			|| x2 > 2 || y2 > 2 || z2 > 2
			|| x1 < 0 || y1 < 0 || z1 < 0
			|| x1 > 2 || y1 > 2 || z1 > 2
		) {
		return false;
	}
	if((x1 == x2)
			&& (y1 == y2)
			&& ((x1 + y1)%2 == 1)
			&& ((z1 == (z2 - 1))
					|| (z1 == (z2 + 1))
				 )
		) {
		return true;
	}
	// if both are on a y axis line
	if((x1 == x2)
			&& (z1 == z2)
			&& ((y1 == (y2 - 1))
				|| (y1 == (y2 + 1))
				)
		) {
		return true;
	}
	// if both are on ax axis line
	if((y1 == y2)
			&& (z1 == z2)
			&& ((x1 == (x2 - 1))
				|| (x1 == (x2 + 1))
				)
		) {
		return true;
	}
	return false;
}
