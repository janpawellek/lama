
#include "arminterface.h"

arminterface::arminterface() {
	connected = false;

	// Create BoardPosition array
	positions = new BoardPosition[29];

	createBoardPosition(&positions[0], 5.5, 5.0, -2.0, 5.0, 7.5, -0.5, 0.1);
	createBoardPosition(&positions[1], 16, 3.0, -2.0, 19.0, 3.5, 0.0, 0.6);
	createBoardPosition(&positions[2], 25.0, 1.0, -2.0, 22.0, 2.5, 0.0, 0.1);
	createBoardPosition(&positions[3], 7.0, 14.0, -1.0, 5.0, 12.0, 0.0, 0.1);

	createBoardPosition(&positions[5], 27.0, 10.0, -1.5, 27.0, 8.0, 0.0, 0.2);
	createBoardPosition(&positions[6], 8.5, 22.5, -2.5, 8.5, 21.0, -1.0, 0.7);
	createBoardPosition(&positions[7], 19.0, 21.0, -2.0, 16.0, 20.0, -1.0, 0.6);
	createBoardPosition(&positions[8], 28.75, 18.5, -2.5, 28.5, 16.0, -1.0, 0.4);
	createBoardPosition(&positions[9], 9.5, 7.5, -2.0, 9.5, 10.0, -0.5, 0.4);
	createBoardPosition(&positions[10], 17.5, 6.0, -2.0, 20.0, 6.5, -0.5, 0.6);
	createBoardPosition(&positions[11], 23.5, 4.5, -2.0, 21.0, 4.0, -0.5, 0.6);
	createBoardPosition(&positions[12], 11.5, 13.25, -2.0, 11.5, 11.5, 0.0, 0.1);

	createBoardPosition(&positions[14], 24.5, 10.5, -2.0, 24.5, 9.0, -0.5, 0.2);
	createBoardPosition(&positions[15], 11.5, 20.0, -2.0, 13.0, 22.0, -0.5, 0.7);
	createBoardPosition(&positions[16], 19.5, 18.5, -2.0, 15.0, 18, -0.5, 0.6);
	createBoardPosition(&positions[17], 25.5, 17.5, -2.0, 26.0, 14.5, -0.5, 0.2);
	createBoardPosition(&positions[18], 14.0, 10.0, -2.0, 13.0, 8.0, -0.5, 0.75);
	createBoardPosition(&positions[19], 17.0, 9.0, -2.0, 17.0, 12.0, -0.5, 0.75);
	createBoardPosition(&positions[20], 21.0, 8.5, -2.0, 19.0, 11.0, -0.5, 0.45);
	createBoardPosition(&positions[21], 14.5, 13.0, -2.0, 18.0, 13.0, -0.5, 0.75);

	createBoardPosition(&positions[23], 21.5, 12.0, -2.0, 18.0, 12.0, -0.5, 0.4);
	createBoardPosition(&positions[24], 15.0, 16.0, -2.0, 16.5, 14.0, -0.5, 0.3);
	createBoardPosition(&positions[25], 18.5, 15.5, -2.0, 18.5, 13.0, -0.5, 0.3);
	createBoardPosition(&positions[26], 22.0, 15.0, -2.0, 22.0, 17.0, -0.5, 0.5);

	createBoardPosition(&positions[POS_SLEEP], 20.0, -6.5, -2.0, 20.0, -6.5, -2.0, 0.5);
	createBoardPosition(&positions[POS_NEW_TOKEN], 24.75, -6.5, -4.0, 22.0, -6.5, -4, 0.1);
}

arminterface::~arminterface() {
	delete a;
	delete[] positions;
	disconnect();
}
	

bool arminterface::connect(char* device) {
	if (!connected) {
		cout << "Baue Verbindung auf ... " << endl;
		a = new Actionfield(device); // baut eine Verbindung mit 6 Servos auf.
		b = a->getServoboard();
		if(!b->isConnected()) {
			return false;
		}

		// muss "unbedingt" gemacht werden.
		a->resetArm();

		connected = true;
		return true;
	}
	return false;
}

bool arminterface::disconnect() {
	if (connected) {
		b->disconnect();
		connected = false;
		return true;
	}
	return false;
}

void arminterface::createBoardPosition(BoardPosition * pos, float x, float y, float z, float x_side, float y_side, float z_side, float rotation) {
	pos->x = x;
	pos->y = y;
	pos->z = z;
	pos->z_above = 3.0;
	pos->x_side = x_side;
	pos->y_side = y_side;
	pos->z_side = z_side;
	pos->rotation = rotation;
}

void arminterface::moveToPos(int pos) {
	(*a).moveFingersToPos(positions[pos].x,
			positions[pos].y,
			positions[pos].z,
			1.0);
}

void arminterface::moveToken(int fromPos, int toPos) {
	if (!connected) {
		std::cout << "Nicht verbunden.";
		return;
	}
	
	// 1. Move above fromPos
	(*a).moveFingersToPos(positions[fromPos].x,
			   positions[fromPos].y,
			   positions[fromPos].z_above,
			   1.0);
	// 1a. Rotate fingers and open
	(*(*a).getServoboard()).servo[5].setPos(positions[fromPos].rotation);
	(*(*a).getServoboard()).updateSimultaneously(500);
	(*a).openFingers();
	// 2. Move to fromPos
	(*a).moveFingersToPos(positions[fromPos].x,
			   positions[fromPos].y,
			   positions[fromPos].z,
			   1.0);
	(*a).closeFingers();
	sleep(1);
	// 3. Move to side position
	(*a).moveFingersToPos(positions[fromPos].x_side,
			   positions[fromPos].y_side,
			   positions[fromPos].z_side,
			   1.0);
	// 4. Move above side position
	(*a).moveFingersToPos(positions[fromPos].x_side,
			   positions[fromPos].y_side,
			   positions[fromPos].z_above,
			   1.0);
	// 5. Move above toPos
	(*a).moveFingersToPos(positions[toPos].x,
			   positions[toPos].y,
			   positions[toPos].z_above,
			   1.0);
	// 5a. Rotate fingers
	(*(*a).getServoboard()).servo[5].setPos(positions[toPos].rotation);
	(*(*a).getServoboard()).updateSimultaneously(500);
	// 6. Move to toPos
	(*a).moveFingersToPos(positions[toPos].x,
			   positions[toPos].y,
			   positions[toPos].z,
			   1.0);
	(*a).openFingers();
	sleep(1);
	// 7. Move above toPos
	(*a).moveFingersToPos(positions[toPos].x,
			   positions[toPos].y,
			   positions[toPos].z_above,
			   1.0);
}


int arminterface::convertToArmPos(int x, int y, int z) {
	return x + 3 * y + 9 * z;
}

