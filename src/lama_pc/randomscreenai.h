#ifndef RANDOMAIARM_H
#define RANDOMAIARM_H

#include"player.h"

#include<iostream>
#include<time.h>
#include<stdlib.h>

class randomScreenAI : public Player {
	private:
		void turnSet();
		void turnMove();
		void turnJump();
		void turnWin();
		void turnLose();

	public:
		randomScreenAI();
		randomScreenAI(Board*);

		void turn(phase);
		void beat();
};

#endif //RANDOMAIARM_H
