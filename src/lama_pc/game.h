#ifndef GAME_H
#define GAME_H

#include"board.h"
#include"player.h"

#include<vector>

class game {
	private:
		struct play {
			Player * player;
			phase state;
			int numTurns;

			play(Player*, phase, int);
		};

		play one;
		play two;

		Board * current;
		Board * last;

		fieldstate next;

		// Methode to let a specific Player
		// make a Turn
		void turn(play&);

		// Methode to undo or confirm
		// a Turn done by a Player
		inline void rewinde();
		inline void confirme();

		// Methodes to check for a specific
		// kind of field transition
		bool meToEmpty(int, int, int, const play&) const;
		bool emptyToMe(int, int, int, const play&) const;

		// Methodes to check if a turn is valid 
		bool validTurn(const play&) const;
		bool validSet(const play&) const;
		bool validMove(const play&) const;
		bool validJump(const play&) const;
		bool validLose(const play&) const;
		bool validWin(const play&) const;

		bool posibleBeat(const play&) const;
		bool validBeat(const play&) const;

		void setNextPhase(play&);

	public:
		game(Player*, Player*);
		~game();

		void nextTurn();

		// phase checkPhase(const play&) const;

		// void newState(Board);

		std::vector<std::vector<int> > diff() const;

		int countStones(fieldstate) const;
};

#endif //GAME_H
