#include"player.h"

/*
Player::Player(const Board * field, fieldstate me) : field(field), me(me) {
}
*/

Player::Player(Board * f) :
		field(f) {
			this->me = EMPTY;
}

Player::Player() {
	this->field = NULL;
	//this->field = new Board();
	this->me = EMPTY;
}

const fieldstate& Player::whoAmI() const {
	return this->me;
}

std::string Player::playerPhase(phase& p) {
	switch(p) {
		case SET:
			return "Setzen";
		case MOVE:
			return "Bewegen";
		case JUMP:
			return "Springen";
		case WIN:
			return "Du hast gewonnen";
		case LOSE:
			return "Du hast verloren";
	}
	return "Irgendetwas ging irgendwo furchtbar schief";
}
