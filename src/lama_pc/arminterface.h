
#ifndef ARMINTERFACE_H
#define ARMINTERFACE_H

# include <iostream>
# include <stdlib.h> 
# include <string>

# include "lynx/servoboard.h"
# include "lynx/actionfield.h"
# include "lynx/serialline.h"

using namespace std;

// Definiert eine Position auf dem Spielfeld
typedef struct {
	float x;
	float y;
	float z;
	float z_above;
	float x_side;
	float y_side;
	float z_side;
	float rotation;
} BoardPosition;

// Position für den Ruhemodus
const int POS_SLEEP = 27;

// Position, um einen neuen Spielstein aufzunehmen
const int POS_NEW_TOKEN = 28;

class arminterface {
	public:
		// constructor
		arminterface();

		// destructor
		~arminterface();

		// conmect to a devicea (e.g. "/dev/ttyS0")
		bool connect(char* device);

		// disconnect
		bool disconnect();

		// Den Arm auf eine Position bewegen
		void moveToPos(int pos);

		// Einen Spielstein von einem Feld auf ein anderes bewegen
		void moveToken(int fromPos, int toPos);

		// Von PC-Koordinaten zu Arm-Positionen umrechnen
		int convertToArmPos(int x, int y, int z);
	private:
		bool connected;
		Actionfield* a;
		BoardPosition* positions;
		Servoboard* b;

		// Hilfsmethode, um Bord-Positionen zu erstellen
		void createBoardPosition(BoardPosition * pos, float x, float y, float z, float x_side, float y_side, float z_side, float rotation);
};

#endif // ARMINTERFACE_H
