#include"randomaiarm.h"

randomAIArm::randomAIArm(
		arminterface * a,
		boardinterface * b
	)
	: Player() {
	this->arm = a;
	this->board = b;
}

randomAIArm::randomAIArm(
		arminterface * a,
		boardinterface * b,
		Board * f
	)
	: Player(f) {
	this->arm = a;
	this->board = b;
}

void randomAIArm::turn(phase moment) {
	srand(time(NULL));
	switch(moment) {
		case SET:
			this->turnSet();
			break;
		case MOVE:
			this->turnMove();
			break;
		case JUMP:
			this->turnJump();
			break;
		case WIN:
			this->turnWin();
			break;
		case LOSE:
			this->turnLose();
			break;
		default:
			break;
	}
	this->field->print();
	return;
}

void randomAIArm::beat() {
	vector<std::vector<int> > enemy = this->field->allOfPlayer(
			this->field->enemyOf(
				this->whoAmI()
			)
		);
	bool found = false;
	int random;
	while(not found) {
		random = rand() % enemy.size();
		int x = enemy[random][0];
		int y = enemy[random][1];
		int z = enemy[random][2];
		if(this->field->isInMill(x, y, z)) {
			enemy.erase(enemy.begin() + random);
			continue;
		}
		else {
			// remove stone from position
			// by moving arm herer
			arm->moveToken(arm->convertToArmPos(x, y, z), POS_SLEEP);
			this->board->getBoardStatus();
			arm->moveToPos(POS_SLEEP);
			this->field = this->board->calculateRealBoard(
					this->field,
					this->whoAmI()
				);
			return;
		}
	}
}

void randomAIArm::turnSet() {
	std::vector<std::vector<int> > empty = this->field->allOfPlayer(
			EMPTY
		);
	int random = rand() % empty.size();
	int x = empty[random][0];
	int y = empty[random][1];
	int z = empty[random][2];
	// set on field x y z
	// by using the arm
	arm->moveToken(POS_NEW_TOKEN, arm->convertToArmPos(x, y, z));
	this->board->getBoardStatus();
	arm->moveToPos(POS_SLEEP);
	this->field = this->board->calculateRealBoard(
			this->field,
			this->whoAmI()
		);
	return;
}

void randomAIArm::turnMove() {
	std::vector<std::vector<int> > me = this->field->allOfPlayer(
			this->whoAmI()
		);
	std::vector<std::vector<int> > neighbor;
	bool found = false;
	int randomOld;
	int randomNew;
	while(not found) {
		randomOld = rand() % me.size();
		int x1 = me[randomOld][0];
		int y1 = me[randomOld][1];
		int z1 = me[randomOld][2];
		neighbor = this->field->neighbors(x1, y1, z1);
		while(neighbor.size() > 0) {
			randomNew = rand() % neighbor.size();
			int x2 = neighbor[randomNew][0];
			int y2 = neighbor[randomNew][1];
			int z2 = neighbor[randomNew][2];
			if((*this->field)[x2][y2][z2] == EMPTY) {
				// take from x1 y1 z1 
				// with the arm
				// move to x2 y2 z2
				arm->moveToken(arm->convertToArmPos(x1, y1, z1),
					arm->convertToArmPos(x2, y2, z2));
				// get the changed board
				this->board->getBoardStatus();
				arm->moveToPos(POS_SLEEP);
				this->field = this->board->calculateRealBoard(
						this->field,
						this->whoAmI()
					);
				return;
			}
			else {
				neighbor.erase(neighbor.begin() + randomNew);
			}
		}
		me.erase(me.begin() + randomOld);
	}
	return;
}

void randomAIArm::turnJump() {
	std::vector<std::vector<int> > me = this->field->allOfPlayer(
			this->whoAmI()
		);
	int random = rand() % me.size();
	int x1 = me[random][0];
	int y1 = me[random][1];
	int z1 = me[random][2];
	std::vector<std::vector<int> > empty = this->field->allOfPlayer(
			EMPTY
		);
	random = rand() % empty.size();
	int x2 = empty[random][0];
	int y2 = empty[random][1];
	int z2 = empty[random][2];
	// take from x1 y1 z1 
	// move to x2 y2 z2
	// with the arm
	arm->moveToken(arm->convertToArmPos(x1, y1, z1),
		arm->convertToArmPos(x2, y2, z2));
	// get the changed board
	this->board->getBoardStatus();
	arm->moveToPos(POS_SLEEP);
	this->field = this->board->calculateRealBoard(
			this->field,
			this->whoAmI()
		);
	return;
}

void randomAIArm::turnWin() {
	exit(EXIT_SUCCESS);
	return;
}

void randomAIArm::turnLose() {
	return;
}
