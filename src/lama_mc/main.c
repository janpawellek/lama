#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include "uart.h"
#include "adc.h"
#include "pwm.h"
#include "timer.h"


void init();

typedef volatile uint8_t PORT;

typedef struct {
	uint8_t current_w;
	uint8_t ground_w;
	PORT * current_p;
	PORT * ground_p;
} Position;

void getStatus(Position* lookup, uint8_t* status) {
	int i;
	for (i = 0; i < 27; i++) {
		// set current
		*(lookup[i].current_p) |= lookup[i].current_w;

		// read state
		status[i] = *(lookup[i].ground_p) & lookup[i].ground_w;

		// unset current
		*(lookup[i].current_p) &= ~lookup[i].current_w;

		// <DEBUG>
		_delay_ms(10);
		// </DEBUG>
	}
}

void getStatusBlocking(Position* lookup, uint8_t* status) {
	int notchanged = 1;
	while (notchanged) {
		int i;
		for (i = 0; i < 27; i++) {
			// set current
			*(lookup[i].current_p) |= lookup[i].current_w;

			// read state
			if (status[i] != (*(lookup[i].ground_p) & lookup[i].ground_w)) {
			// TODO: HACK: Im Moment lesen wir den Zustand zwei Mal aus,
			// um Schwankungen zu vermeiden --> besser/anders umsetzen
			if (status[i] != (*(lookup[i].ground_p) & lookup[i].ground_w)) {
				status[i] = *(lookup[i].ground_p) & lookup[i].ground_w; 
				notchanged = 0;
			}
			}

			// unset current
			*(lookup[i].current_p) &= ~lookup[i].current_w;
		}
	}
}

void createPosition(
		Position * position,
		uint8_t current_shift,
		uint8_t ground_shift,
		PORT * current_p,
		PORT * ground_p
	) {
	position->current_w = 1 << current_shift;
	position->ground_w = 1 << ground_shift;
	position->current_p = current_p;
	position->ground_p = ground_p;
}

void sendStatus(uint8_t* status) {
	int i;
	for(i = 0; i < 27; i += 1) {
		if (status[i]) {
			uart_puti(i+1);
		}
		else {
			uart_puti(-(i+1));
		}
		uart_puts(" ");
	}
	uart_puts("\r\n");
}

int main(void) {
	//Initialisierung ausfuehren
	init();

	// Create arrays for the board status
	Position lookup[27];
	uint8_t status[27];

	createPosition(&lookup[0],  0, 0, &PORTB, &PINC);
	createPosition(&lookup[1],  3, 0, &PORTB, &PINC);
	createPosition(&lookup[2],  7, 0, &PORTD, &PINC);
	createPosition(&lookup[3],  0, 3, &PORTB, &PINC);
	createPosition(&lookup[4],  3, 3, &PORTB, &PINC); // nicht belegt
	createPosition(&lookup[5],  7, 3, &PORTD, &PINC);
	createPosition(&lookup[6],  0, 6, &PORTB, &PIND);
	createPosition(&lookup[7],  3, 6, &PORTB, &PIND);
	createPosition(&lookup[8],  7, 6, &PORTD, &PIND);
	createPosition(&lookup[9],  1, 1, &PORTB, &PINC);
	createPosition(&lookup[10], 3, 1, &PORTB, &PINC);
	createPosition(&lookup[11], 5, 1, &PORTB, &PINC);
	createPosition(&lookup[12], 1, 3, &PORTB, &PINC);
	createPosition(&lookup[13], 3, 3, &PORTB, &PINC); // nicht belegt
	createPosition(&lookup[14], 5, 3, &PORTB, &PINC);
	createPosition(&lookup[15], 1, 5, &PORTB, &PINC);
	createPosition(&lookup[16], 3, 5, &PORTB, &PINC);
	createPosition(&lookup[17], 5, 5, &PORTB, &PINC);
	createPosition(&lookup[18], 2, 2, &PORTB, &PINC);
	createPosition(&lookup[19], 3, 2, &PORTB, &PINC);
	createPosition(&lookup[20], 4, 2, &PORTB, &PINC);
	createPosition(&lookup[21], 2, 3, &PORTB, &PINC);
	createPosition(&lookup[22], 3, 3, &PORTB, &PINC); // nicht belegt
	createPosition(&lookup[23], 4, 3, &PORTB, &PINC);
	createPosition(&lookup[24], 2, 4, &PORTB, &PINC);
	createPosition(&lookup[25], 3, 4, &PORTB, &PINC);
	createPosition(&lookup[26], 4, 4, &PORTB, &PINC);

	// Set PB0, ..., PB5, PD7 to OUTPUT
	DDRB |= (1<<0);
	DDRB |= (1<<1);
	DDRB |= (1<<2);
	DDRB |= (1<<3);
	DDRB |= (1<<4);
	DDRB |= (1<<5);
	DDRD |= (1<<7);

	// Set PC0, ..., PC5, PD6 to INPUT
	DDRC &= ~(1<<0);
	DDRC &= ~(1<<1);
	DDRC &= ~(1<<2);
	DDRC &= ~(1<<3);
	DDRC &= ~(1<<4);
	DDRC &= ~(1<<5);
	DDRD &= ~(1<<6);

	// read initial status
	getStatus(lookup, status);

	uart_puts("Gestartet\r\n");

	// TEST CODE -------------------------------------
	// variable where the command to be executed is stored
	// posible values are:
	//   'n' => send the status after the next change from now on
	//   'w' => wait for change and then return status
	//          returns emediatly if board has changed sins last check
	//   's' => send the status
	//   'c' => send an answer to confirme conection
	//   ... => more to come if necessary
	unsigned char command = '\0';
	while (1) {
		//getStatus(lookup, status);
		//getStatusBlocking(lookup, status);
		//sendStatus(status);
		command = uart_getc();
		switch(command) {
			case 'n':
				getStatus(lookup, status);
				getStatusBlocking(lookup, status);
				sendStatus(status);
				break;
			case 'w':
				getStatusBlocking(lookup, status);
				sendStatus(status);
				break;
			case 's':
				getStatus(lookup, status);
				sendStatus(status);
				break;
			case 'c':
				uart_puts("Yes i am still here!\r\n");
				break;
			default:
				break;
		}
	}
	return 0;
}

//INIT
void init() {
	uartInit();   // serielle Ausgabe an PC
	ADCInit(0);   // Analoge Werte einlesen
	PWMInit();    // Pulsweite auf D6 ausgeben 
	timerInit();  // "Systemzeit" initialisieren
}
