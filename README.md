
# LaMA – Lynx als Mühle-Arm

## PC-Programm

[Source-Verzeichnis](./src/lama_pc)

* lama.cpp: Hauptprogramm
* game.cpp: Spiellogik
* board.cpp: Zustand des Spielbretts
* player.cpp: Abstrakte Klasse für einen Spieler
* ai.cpp: erbt von Player, Mühle-KI-Spieler
* human.cpp: erbt von Player, menschlicher Spieler
* arminterface.cpp: Schnittstelle zum Greifarm-Microcontroller
* boardinterface.cpp: Schnittstelle zum Board-Microcontroller

## Microcontroller-Programm

[Source-Verzeichnis](./src/lama_mc)

* main.c: Hauptprogramm
